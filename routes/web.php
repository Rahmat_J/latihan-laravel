<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/form', 'AuthController@form');
Route::get('/welcome', 'AuthController@welcome');

Route::get('/hello', function(){
    return view('hello');
});

// Route::get('/master', function(){
//     return view('layout.master');
// });

Route::get('/data-table', function(){
    return view('table.data-table');
});



//CRUD Berita
Route::resource('berita', 'BeritaController');


Route::group(['middleware' => ['auth']], function () {
    // CRUD Kategori
    Route::get('/kategori', 'kategoriController@index');
    Route::get('/kategori/create', 'KategoriController@create');
    Route::post('/kategori', 'KategoriController@store');
    //
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
