@extends('layout.master')

@section('judul')
Halaman Berita
@endsection


@section('content')
@auth
<a href="/berita/create" class="btn btn-primary my-2">Tambah</a>
@endauth
<div class="row">
    @forelse ($berita as $item)
    <div class="col-4">
        <div class="card">
            <img class="card-img-top" src="{{asset('gambar/'. $item->thumbnail)}}" class="img-thumbnail" alt="...">
            <div class="card-body">
              <h2>{{$item->judul}}</h2>
              <p class="card-text">{{Str::limit($item->content, 30)}}</p>
            
            @auth    
                <form action="/berita/{{$item->id}}" method="POST">
                    <a href="/berita/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
                    <a href="/berita/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                    @csrf
                    @method('delete')
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                </form>
                @endauth
                
                @guest
                <a href="/berita/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
                @endguest

            </div>
          </div>
    </div>
        
    @empty
        <h4>Data Berita Belum Ada</h4>
    @endforelse
</div>

@endsection