@extends('layout.master')

@section('judul')
    Halaman Detail
@endsection

@section('content')
    
<img src="{{asset('gambar/'.$berita->thumbnail)}}" alt="">
<h1>{{$berita->judul}}</h1>
<p>{{$berita->content}}></p>

<a href="/berita" class="btn btn-secondary">Kembali</a>
@endsection