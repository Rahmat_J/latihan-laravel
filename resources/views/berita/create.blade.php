@extends('layout.master')

@section('judul')
Halaman Tambah Berita
@endsection

@section('content')
     
<form action="/berita" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label>Judul Berita</label>
      <input type="text" name="judul" class="form-control">
    </div>
    @error('judul')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>content</label>
      <textarea name="content" class="form-control" id="" cols="30" rows="10"></textarea>
    </div>
    @error('content')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Kategori</label>
      <select name="kategori_id" class="form-control" id="">
        <option value="">---Pilih---</option>

        @foreach ($kategori as $item)
            <option value="{{$item->id}}">{{$item->nama}}</option>
        @endforeach

      </select>
    </div>
    @error('kategori_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Thumbnail</label>
      <input type="file" name="thumbnail" class="form-control">
    </div>
    @error('thumbnail')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection