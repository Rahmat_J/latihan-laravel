@extends('layout.master')

@section('judul')
    Halaman Form
@endsection

@section('content')
    
<div>
    <h1>
            <b>Buat Account Baru!</b>
        </h1>
        <p>
            <b>
                Sign Up Form
            </b>
        </p>
    </div>
    <div>
        <form action='/welcome'>
            <div>
                <label for="fname">First Name:</label><br><br>
                <input type="text" id="fname" name="fname"><br><br>
            </div>
            <div>
                <label for="lname">Last name:</label><br><br>
                <input type="text" id="lname" name="lname"><br>
            </div>
            <div>
                <p>Gender</p>
                <input type="radio" id="male" name="gender" value="Male">
                <label for="male">Male</label><br>
                <input type="radio" id="female" name="gender" value="Female">
                <label for="female">Female</label><br>
                <input type="radio" id="other" name="gender" value="Other">
                <label for="other">Other</label><br>
            </div>
            <div>
                <p>Nationality</p>
                <select name="nationality" id="nationality">
                    <option value="indonesia">Indonesia</option>
                    <option value="america">America</option>
                    <option value="english">Inggris</option>
                </select>
            </div>
            <div>
                <p>Language Spoken:</p>
                <input type="checkbox" id="bhsIndonesia" name="bhsIndonesia">
                <label for="bhsIndonesia">Bahasa Indonesia</label><br>
                <input type="checkbox" id="bhsEnglish" name="bhsEnglish">
                <label for="bhsEnglish">English</label><br>
                <input type="checkbox" id="bhsOther" name="bhsOther">
                <label for="bhsOther">Other</label>
            </div>
            <div>
                <p>Bio:</p>
                <textarea cols="25" rows="10"></textarea>
            </div>
            <div>
                <input type="submit" value="Sign Up">
            </div>
        </form>
    </div>
    
    @endsection