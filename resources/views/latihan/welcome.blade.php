@extends('layout.master')

@section('content')
    
<div>
    <h1>Selamat Datang! {{$firstName}} {{$lastName}}</h1>
    <p>
        <b>Terima Kasih Sudah Bergabung Di Website kami. Media Belajar Kita Bersama!</b>
    </p>
</div>
@endsection