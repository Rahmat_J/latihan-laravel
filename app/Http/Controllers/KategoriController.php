<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;


class KategoriController extends Controller
{
    public function create(){
        return view('kategori.create');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
            'deskripsi' => 'required',
        ]);

        DB::table('kategori')->insert([
            'nama' => $request['nama'], 
            'deskripsi' => $request['deskripsi']
        ]);

        return redirect('/kategori');
    }

    public function index(Request $request){
        $kategori = DB::table('kategori')->get();

        return view('kategori.index', compact('kategori'));
    }
}
